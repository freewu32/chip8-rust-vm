# 使用Rust开发的chip-8游戏机模拟器

## 参考

http://www.codeslinger.co.uk/pages/projects/chip8/mychip8.html

## 如何使用

**构造函数传入rom路径**
```rust
#[macroquad::main("chip8")]
async fn main() {
    let mut g = Chip8Game::new("rom/game/Worm V4 [RB-Revival Studios, 2007].ch8");
    g.run().await;
}
```

## 模拟器界面

![贪吃蛇](image/worm.png)