use std::fs::File;
use std::io::Read;

use rand::random;
use typed_index_collections::TiVec;

pub const SCREEN_WIDTH: u8 = 64;
pub const SCREEN_HEIGHT: u8 = 32;
const ROM_SIZE: usize = 0xfff;
const PC_START: usize = 0x200;

/// chip-8 虚拟机
pub struct VM {
    /// 内存
    memory: TiVec<u16, u8>,
    /// 数据寄存器
    reg_data: TiVec<u16, u8>,
    /// 地址寄存器
    reg_address: u16,
    /// 程序计数器
    pc: u16,
    /// 栈内存
    stack: TiVec<u16, u16>,
    /// 屏幕数据
    screen: TiVec<u8, TiVec<u8, bool>>,
    /// 按键状态
    keys: TiVec<u8, u8>,
    /// 延时计时器
    delay_timer: u8,
    /// 音频计时器
    sound_timer: u8,
}

impl VM {
    /// 初始化虚拟机
    pub fn new() -> Self {
        Self {
            memory: TiVec::from(vec![0; ROM_SIZE]),
            reg_data: TiVec::from(vec![0; 16]),
            reg_address: 0,
            pc: 0,
            stack: TiVec::new(),
            screen: TiVec::from(vec![TiVec::from(vec![false; SCREEN_WIDTH as usize]);
                                     SCREEN_HEIGHT as usize]),
            keys: TiVec::from(vec![0; 16]),
            delay_timer: 0,
            sound_timer: 0,
        }
    }

    /// 载入rom
    pub fn load_rom(&mut self, path: &str) {
        self.cpu_reset();
        self.clear_screen();
        // 读取rom文件
        let mut rom = File::open(path).unwrap();
        let mut rom_buffer = Vec::<u8>::new();
        rom.read_to_end(&mut rom_buffer).unwrap();
        let raw_slice = &mut self.memory.raw[PC_START..PC_START + rom_buffer.len()];
        raw_slice.copy_from_slice(&rom_buffer);
    }

    /// 递减计时器
    pub fn decrease_timers(&mut self) {
        if self.delay_timer > 0 {
            self.delay_timer -= 1;
        }
        if self.sound_timer > 0 {
            self.sound_timer -= 1
        }
        if self.sound_timer > 0 {
            // TODO sound
        }
    }

    /// 按下按键
    pub fn key_pressed(&mut self, key: u8) {
        if self.keys[key] == 1 {
            return;
        }
        println!("press {:?}",key);
        self.keys[key] = 1;
    }

    /// 释放所有按键
    pub fn key_released(&mut self, key: u8) {
        println!("release {:?}",key);
        self.keys[key] = 0;
    }

    /// 执行操作码
    pub fn execute_opcode(&mut self) {
        let opcode = self.get_opcode();
        // println!("pc={:x} opcode={:x}", self.pc, opcode);
        match opcode & 0xF000 {
            0x0000 => self.decode_opcode0(opcode),
            0x1000 => self.opcode_1nnn(opcode),
            0x2000 => self.opcode_2nnn(opcode),
            0x3000 => self.opcode_3xnn(opcode),
            0x4000 => self.opcode_4xnn(opcode),
            0x5000 => self.opcode_5xy0(opcode),
            0x6000 => self.opcode_6xnn(opcode),
            0x7000 => self.opcode_7xnn(opcode),
            0x8000 => self.decode_opcode8(opcode),
            0x9000 => self.opcode_9xy0(opcode),
            0xA000 => self.opcode_annn(opcode),
            0xB000 => self.opcode_bnnn(opcode),
            0xC000 => self.opcode_cxnn(opcode),
            0xD000 => self.opcode_dxyn(opcode),
            0xE000 => self.decode_opcode_e(opcode),
            0xF000 => self.decode_opcode_f(opcode),
            _ => return
        }
    }

    pub fn screen(&mut self) -> &TiVec<u8, TiVec<u8, bool>> {
        &self.screen
    }

    /// 重置cpu
    fn cpu_reset(&mut self) {
        self.reg_address = 0;
        self.pc = PC_START as u16;
        self.reg_data.raw.fill(0);
        self.memory.raw.fill(0);
        self.keys.raw.fill(0);
        self.delay_timer = 0;
        self.sound_timer = 0;
    }

    /// 清理屏幕
    fn clear_screen(&mut self) {
        for x in 0..SCREEN_WIDTH {
            for y in 0..SCREEN_HEIGHT {
                self.screen[y][x] = false;
            }
        }
    }

    /// 获取被按下的键
    fn get_key_pressed(&mut self) -> Option<u8> {
        for i in 0..16 {
            if self.keys[i] > 0 {
                return Some(i);
            }
        }
        return None;
    }

    /// 获取当前操作码
    fn get_opcode(&mut self) -> u16 {
        let mut opcode = self.memory[self.pc] as u16;
        opcode <<= 8;
        opcode |= self.memory[self.pc + 1] as u16;
        self.pc += 2;
        return opcode;
    }

    fn decode_opcode0(&mut self, opcode: u16) {
        match opcode & 0xF {
            0x0 => self.clear_screen(),
            0xE => self.opcode_00ee(),
            _ => return,
        }
    }

    fn decode_opcode8(&mut self, opcode: u16) {
        match opcode & 0xF {
            0x0 => self.opcode_8xy0(opcode),
            0x1 => self.opcode_8xy1(opcode),
            0x2 => self.opcode_8xy2(opcode),
            0x3 => self.opcode_8xy3(opcode),
            0x4 => self.opcode_8xy4(opcode),
            0x5 => self.opcode_8xy5(opcode),
            0x6 => self.opcode_8xy6(opcode),
            0x7 => self.opcode_8xy7(opcode),
            0xE => self.opcode_8xye(opcode),
            _ => return
        }
    }

    fn decode_opcode_e(&mut self, opcode: u16) {
        match opcode & 0xF {
            0xE => self.opcode_ex9e(opcode),
            0x1 => self.opcode_exa1(opcode),
            _ => return
        }
    }

    fn decode_opcode_f(&mut self, opcode: u16) {
        match opcode & 0xFF {
            0x07 => self.opcode_fx07(opcode),
            0x0A => self.opcode_fx0a(opcode),
            0x15 => self.opcode_fx15(opcode),
            0x18 => self.opcode_fx18(opcode),
            0x1E => self.opcode_fx1e(opcode),
            0x29 => self.opcode_fx29(opcode),
            0x33 => self.opcode_fx33(opcode),
            0x55 => self.opcode_fx55(opcode),
            0x65 => self.opcode_fx65(opcode),
            _ => return
        }
    }

    #[inline]
    fn get_regx(&mut self, opcode: u16) -> u16 {
        let mut regx = opcode & 0x0F00;
        regx >>= 8;
        return regx;
    }

    #[inline]
    fn get_regy(&mut self, opcode: u16) -> u16 {
        let mut regy = opcode & 0x00F0;
        regy >>= 4;
        return regy;
    }

    #[inline]
    fn get_reg_xy(&mut self, opcode: u16) -> (u16, u16) {
        (self.get_regx(opcode), self.get_regy(opcode))
    }

    /// return from subroutine
    fn opcode_00ee(&mut self) {
        self.pc = self.stack.pop().unwrap();
    }

    /// jump to address NNN
    fn opcode_1nnn(&mut self, opcode: u16) {
        self.pc = opcode & 0x0FFF;
    }

    /// call subroutine NNN
    fn opcode_2nnn(&mut self, opcode: u16) {
        self.stack.push(self.pc);
        self.pc = opcode & 0x0FFF;
    }

    /// skip next instruction if VX == NN
    fn opcode_3xnn(&mut self, opcode: u16) {
        let nn = (opcode & 0x00FF) as u8;
        let regx = self.get_regx(opcode);
        if self.reg_data[regx] == nn {
            self.pc += 2;
        }
    }

    /// skip next instruction if VX != NN
    fn opcode_4xnn(&mut self, opcode: u16) {
        let nn = (opcode & 0x00FF) as u8;
        let regx = self.get_regx(opcode);
        if self.reg_data[regx] != nn {
            self.pc += 2;
        }
    }

    /// skip next instruction if VX == VY
    fn opcode_5xy0(&mut self, opcode: u16) {
        let (regx, regy) = self.get_reg_xy(opcode);
        if self.reg_data[regx] == self.reg_data[regy] {
            self.pc += 2;
        }
    }

    /// sets VX to nn
    fn opcode_6xnn(&mut self, opcode: u16) {
        let nn = opcode & 0x00FF;
        let regx = self.get_regx(opcode);
        self.reg_data[regx] = nn as u8;
    }

    /// adds NN to vx. carry not affected
    fn opcode_7xnn(&mut self, opcode: u16) {
        let nn = opcode & 0x00FF;
        let regx = self.get_regx(opcode);
        self.reg_data[regx] = self.reg_data[regx].wrapping_add(nn as u8);
    }

    /// set vx to vy
    fn opcode_8xy0(&mut self, opcode: u16) {
        let (regx, regy) = self.get_reg_xy(opcode);
        self.reg_data[regx] = self.reg_data[regy];
    }

    /// VX = VX | VY
    fn opcode_8xy1(&mut self, opcode: u16) {
        let (regx, regy) = self.get_reg_xy(opcode);
        self.reg_data[regx] = self.reg_data[regx] | self.reg_data[regy];
        self.reg_data[0xf] = 0;
    }

    /// VX = VX & VY
    fn opcode_8xy2(&mut self, opcode: u16) {
        let (regx, regy) = self.get_reg_xy(opcode);
        self.reg_data[regx] = self.reg_data[regx] & self.reg_data[regy];
        self.reg_data[0xf] = 0;
    }

    /// VX = VX xor VY
    fn opcode_8xy3(&mut self, opcode: u16) {
        let (regx, regy) = self.get_reg_xy(opcode);
        self.reg_data[regx] = self.reg_data[regx] ^ self.reg_data[regy];
        self.reg_data[0xf] = 0;
    }

    /// add vy to vx. set carry to 1 if overflow otherwise 0
    fn opcode_8xy4(&mut self, opcode: u16) {
        let (regx, regy) = self.get_reg_xy(opcode);
        let mut carry: u8 = 0;
        if self.reg_data[regx] as u16 + self.reg_data[regy] as u16 > 255 {
            carry = 1;
        }
        self.reg_data[regx] = self.reg_data[regx].wrapping_add(self.reg_data[regy]);
        self.reg_data[0xF] = carry;
    }

    /// sub vy from vx. set carry to 1 if no borrow otherwise 0
    fn opcode_8xy5(&mut self, opcode: u16) {
        let (regx, regy) = self.get_reg_xy(opcode);
        let mut carry: u8 = 1;
        if self.reg_data[regy] > self.reg_data[regx] {
            carry = 0;
        }
        self.reg_data[regx] = self.reg_data[regx].wrapping_sub(self.reg_data[regy]);
        self.reg_data[0xF] = carry;
    }

    /// Shifts VX right by one. VF is set to the value of the least significant
    /// bit of VX before the shift.
    fn opcode_8xy6(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);
        let carry = self.reg_data[regx] & 0x1;
        self.reg_data[regx] >>= 1;
        self.reg_data[0xF] = carry;
    }

    /// Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
    fn opcode_8xy7(&mut self, opcode: u16) {
        let (regx, regy) = self.get_reg_xy(opcode);
        let mut carry : u8 = 1;
        if self.reg_data[regy] < self.reg_data[regx] {
            carry = 0;
        }
        self.reg_data[regx] = self.reg_data[regy].wrapping_sub(self.reg_data[regx]);
        self.reg_data[0xF] = carry;
    }

    /// Shifts VX left by one. VF is set to the value of the most
    /// significant bit of VX before the shift
    fn opcode_8xye(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);
        let carry = self.reg_data[regx] >> 7;
        self.reg_data[regx] <<= 1;
        self.reg_data[0xF] = carry;
    }

    /// skip next instruction if VX != VY
    fn opcode_9xy0(&mut self, opcode: u16) {
        let (regx, regy) = self.get_reg_xy(opcode);
        if self.reg_data[regx] != self.reg_data[regy] {
            self.pc += 2;
        }
    }

    /// set I to nnn
    fn opcode_annn(&mut self, opcode: u16) {
        self.reg_address = opcode & 0x0FFF;
    }

    /// jump to address NNN + V0
    fn opcode_bnnn(&mut self, opcode: u16) {
        let nnn = opcode & 0x0FFF;
        let x = (opcode & 0x0F00) >> 8;
        if x == 0 {
            self.pc = self.reg_data[0] as u16 + nnn;
        } else {
            self.pc = nnn + self.reg_data[x] as u16;
        }
    }

    /// set vx to rand + NN
    fn opcode_cxnn(&mut self, opcode: u16) {
        let nn = opcode & 0x00FF;
        let regx = self.get_regx(opcode);
        self.reg_data[regx] = (random::<u16>() & nn) as u8;
    }

    /// Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N pixels.
    /// As described above, VF is set to 1 if any screen pixels are flipped from set to unset when
    /// the sprite is drawn, and to 0 if that doesn't happen
    fn opcode_dxyn(&mut self, opcode: u16) {
        let (regx, regy) = self.get_reg_xy(opcode);
        let coordx = self.reg_data[regx];
        let coordy = self.reg_data[regy];
        let height = opcode & 0x000F;

        self.reg_data[0xF] = 0;

        for y_line in 0..height {
            let pixel = self.memory[self.reg_address + y_line];

            for x_line in 0..8 {
                if (pixel & (0x80 >> x_line)) != 0 {
                    let mut x = (x_line as u8).wrapping_add(coordx);
                    let mut y = coordy.wrapping_add(y_line as u8);
                    x %= SCREEN_WIDTH;
                    y %= SCREEN_HEIGHT;

                    if self.screen[y][x] {
                        self.reg_data[0xf] = 1;
                    }

                    self.screen[y][x] ^= true;
                }
            }
        }
    }

    /// Skips the next instruction if the key stored in VX is pressed.
    fn opcode_ex9e(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);
        let key = self.reg_data[regx];
        if self.keys[key] == 1 {
            self.pc += 2;
        }
    }

    /// Skips the next instruction if the key stored in VX isn't pressed.
    fn opcode_exa1(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);
        let key = self.reg_data[regx];
        if self.keys[key] == 0 {
            self.pc += 2;
        }
    }

    /// Sets VX to the value of the delay timer.
    fn opcode_fx07(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);
        self.reg_data[regx] = self.delay_timer;
    }

    /// A key press is awaited, and then stored in VX.
    fn opcode_fx0a(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);

        match self.get_key_pressed() {
            Some(key_pressed) => {
                self.reg_data[regx] = key_pressed;
            }
            None => {
                self.pc -= 2;
            }
        }
    }

    /// delay to vx
    fn opcode_fx15(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);
        self.delay_timer = self.reg_data[regx];
    }

    /// sound to vx
    fn opcode_fx18(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);
        self.sound_timer = self.reg_data[regx];
    }

    /// adds vx to I
    fn opcode_fx1e(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);
        self.reg_address += self.reg_data[regx] as u16;
    }

    /// Sets I to the location of the sprite for the character in VX.
    /// Characters 0-F (in hexadecimal) are represented by a 4x5 font.
    fn opcode_fx29(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);
        self.reg_address = self.reg_data[regx] as u16 * 5;
    }

    /// Stores the Binary-coded decimal representation of VX at the addresses I,
    /// I plus 1, and I plus 2.
    fn opcode_fx33(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);
        let value = self.reg_data[regx];

        let hundreds = value / 100;
        let tens = (value / 10) % 10;
        let units = value % 10;

        self.memory[self.reg_address] = hundreds;
        self.memory[self.reg_address + 1] = tens;
        self.memory[self.reg_address + 2] = units;
    }

    /// Stores V0 to VX in memory starting at address I.
    fn opcode_fx55(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);
        for i in 0..=regx {
            self.memory[self.reg_address + i] = self.reg_data[i];
        }
        self.reg_address = self.reg_address + regx + 1;
    }

    /// Fills V0 to VX with values from memory starting at address I.
    fn opcode_fx65(&mut self, opcode: u16) {
        let regx = self.get_regx(opcode);
        for i in 0..=regx {
            self.reg_data[i] = self.memory[self.reg_address + i];
        }
        self.reg_address = self.reg_address + regx + 1;
    }
}

mod test {
    use crate::vm::VM;

    #[test]
    fn test_load_rom() {
        let mut vm = VM::new();
        vm.load_rom("rom/test_opcode.ch8");
        vm.decrease_timers();
        vm.execute_opcode();
        vm.execute_opcode();
    }
}