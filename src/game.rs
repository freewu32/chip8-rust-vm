use macroquad::miniquad::CullFace::Back;
use macroquad::prelude::*;

use crate::vm::{SCREEN_HEIGHT, SCREEN_WIDTH, VM};

/// 一秒执行多少指令
const NUM_OPCODES: i32 = 400;

/// 上次执行代码时间
const EXECUTE_INTERVAL: f64 = 1000.0;

/// 映射的键盘编码
const KEY_CODES: [KeyCode; 16] = [
    KeyCode::Key0, KeyCode::Key1, KeyCode::Key2, KeyCode::Key3,
    KeyCode::Key4, KeyCode::Key5, KeyCode::Key6, KeyCode::Key7,
    KeyCode::Key8, KeyCode::Key9, KeyCode::A, KeyCode::B,
    KeyCode::C, KeyCode::D, KeyCode::E, KeyCode::F
];

pub struct Chip8Game {
    vm: VM,
}

impl Chip8Game {
    /// 使用rom路径初始化游戏
    pub fn new(path: &str) -> Self {
        let mut vm = VM::new();
        vm.load_rom(path);
        Self {
            vm,
        }
    }

    /// 运行游戏
    pub async fn run(&mut self) {
        request_new_screen_size(640., 320.);
        next_frame().await;

        let mut image = Image::gen_image_color(screen_width() as u16,
                                               screen_height() as u16, WHITE);

        let texture = Texture2D::from_image(&image);

        // 上次渲染时间
        let mut last_execute_time = get_time();

        loop {
            self.handle_input_event();

            last_execute_time = self.execute_code_in_game(last_execute_time);

            self.rendering(&mut image);

            texture.update(&image);

            draw_texture(texture, 0., 0., WHITE);

            next_frame().await
        }
    }

    /// 处理输入事件
    fn handle_input_event(&mut self) {
        for (index, code) in KEY_CODES.iter().enumerate() {
            let code = *code;
            if is_key_down(code) {
                self.vm.key_pressed(index as u8);
            }
            if is_key_released(code) {
                self.vm.key_released(index as u8);
            }
        }
    }

    /// 在游戏中执行操作码
    fn execute_code_in_game(&mut self, last_execute_time: f64) -> f64 {
        let current_time = get_time();

        if current_time > last_execute_time + EXECUTE_INTERVAL {
            return last_execute_time;
        }

        let mut fps = get_fps();
        if fps == 0 {
            fps = 60;
        }
        // 一帧执行多少指令
        let num_frame = NUM_OPCODES / fps;

        self.vm.decrease_timers();

        for _ in 0..num_frame {
            self.vm.execute_opcode();
        }

        return current_time;
    }

    /// 渲染图片
    fn rendering(&mut self, image: &mut Image) {
        clear_background(WHITE);

        let w = screen_width() as usize;
        let h = screen_height() as usize;
        let scale = w / SCREEN_WIDTH as usize;

        let screen = self.vm.screen();
        // 对新图像的每个像素计算插值
        for i in 0..h {
            for j in 0..w {
                // 计算新像素在原图像中的坐标
                let x = j / scale;
                let y = i / scale;
                if x as u8 >= SCREEN_WIDTH || y as u8 >= SCREEN_HEIGHT {
                    continue;
                }
                let color = screen[y as u8][x as u8];
                if color {
                    image.set_pixel(j as u32, i as u32, BLACK);
                } else {
                    image.set_pixel(j as u32, i as u32, WHITE);
                }
            }
        }
    }
}