use crate::game::Chip8Game;

pub mod vm;
pub mod game;

#[macroquad::main("chip8")]
async fn main() {
    let mut g = Chip8Game::new("rom/unit/1-chip8-logo.ch8");
    g.run().await;
}
